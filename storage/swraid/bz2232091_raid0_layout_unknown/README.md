# storage/swraid/bz2232091_raid0_layout_unknown

Test the function of RAID 0 default layout is "-unknown-"

## How to run it
Please refer to the top-level README.md for common dependencies.

### Install dependencies
```bash
root# bash ../../../cki_bin/pkgs_install.sh metadata
```

### Execute the test
```bash
bash ./runtest.sh
```
